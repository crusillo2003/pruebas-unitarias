package com.mx.bank.bean;

import lombok.Data;

@Data
public class Account {

	private int accounId;
	private Double amount;
}
