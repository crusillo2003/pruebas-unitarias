package com.mx.bank.services;

import java.util.List;

import com.mx.bank.bean.Account;

public interface AccountService {
	
	public Account getAccountByAccountNumber(int accountNumber);
	public List<Account> getAccountByUserId(int userId);

}
