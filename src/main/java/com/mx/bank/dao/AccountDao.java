package com.mx.bank.dao;

import com.mx.bank.bean.Account;

public interface AccountDao {
	
	Account getAccountById(int id);

}
